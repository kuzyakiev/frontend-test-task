module.exports = (grunt) ->

  stylesTasks = []
  jsTasks = ['concat']

  grunt.initConfig
    pkg: grunt.file.readJSON 'package.json'

    concat:
      options:
        separator: ';'
        stripBanners: true
        banner: '/*! Oleg Kudrenko <oleg.kudrenko@gmail.com> */',
      dist:
        files:
          'static/js/greenwallet/App/app.min.js': ['static/js/greenwallet/App/app.js','static/js/greenwallet/App/*/*.js']
          'static/js/greenwallet/BaseApp/app.min.js': ['static/js/greenwallet/BaseApp/app.js','static/js/greenwallet/BaseApp/*/*.js']
          'static/js/greenwallet/Info/app.min.js': ['static/js/greenwallet/Info/app.js','static/js/greenwallet/Info/*/*.js']
          'static/js/greenwallet/Mnemonics/app.min.js': ['static/js/greenwallet/Mnemonics/app.js','static/js/greenwallet/Mnemonics/*/*.js']
          'static/js/greenwallet/Receive/app.min.js': ['static/js/greenwallet/Receive/app.js','static/js/greenwallet/Receive/*/*.js']
          'static/js/greenwallet/Send/app.min.js': ['static/js/greenwallet/Send/app.js','static/js/greenwallet/Send/*/*.js']
          'static/js/greenwallet/Settings/app.min.js': ['static/js/greenwallet/Settings/app.js','static/js/greenwallet/Settings/*/*.js']
          #'static/js/greenwallet/Signup/app.min.js': ['static/js/greenwallet/Signup/app.js','static/js/greenwallet/Signup/*/*.js']
          'static/js/greenwallet/SignupLogin/app.min.js': ['static/js/greenwallet/SignupLogin/app.js','static/js/greenwallet/SignupLogin/*/*.js']
          'static/js/greenwallet/Transactions/app.min.js': ['static/js/greenwallet/Transactions/app.js','static/js/greenwallet/Transactions/*/*.js']
          #
          'static/js/jsqrcode.min.js': ['static/js/jsqrcode/grid.js', 'static/js/jsqrcode/version.js', 'static/js/jsqrcode/detector.js', 'static/js/jsqrcode/formatinf.js', 'static/js/jsqrcode/errorlevel.js', 'static/js/jsqrcode/bitmat.js', 'static/js/jsqrcode/datablock.js', 'static/js/jsqrcode/bmparser.js', 'static/js/jsqrcode/datamask.js', 'static/js/jsqrcode/rsdecoder.js', 'static/js/jsqrcode/gf256poly.js', 'static/js/jsqrcode/gf256.js', 'static/js/jsqrcode/decoder.js', 'static/js/jsqrcode/qrcode.js', 'static/js/jsqrcode/findpat.js', 'static/js/jsqrcode/alignpat.js', 'static/js/jsqrcode/databr.js']

    watch:
      js:
        files: ['static/js/greenwallet/App/*.*', 'static/js/greenwallet/App/*/*.*']
        tasks: jsTasks


  #grunt.loadNpmTasks 'grunt-contrib-compass'
  #grunt.loadNpmTasks 'grunt-contrib-coffee'
  grunt.loadNpmTasks 'grunt-contrib-concat'
  #grunt.loadNpmTasks 'grunt-contrib-cssmin'
  #grunt.loadNpmTasks 'grunt-contrib-uglify'
  grunt.loadNpmTasks 'grunt-contrib-watch'

  grunt.registerTask 'default', stylesTasks.concat jsTasks, 'watch'
