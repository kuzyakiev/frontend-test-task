/**
 * Created by PhpStorm.
 * User: Oleg Kudrenko <oleg.kudrenko@gmail.com>
 * Date: 7/28/14
 */
greenWalletApp
    .directive("toggleClass", function() {
        return {
            restrict: 'A',
            require: '^toggleableMenu',
            link: function(scope, element, attrs, toggleableMenuController) {
                toggleableMenuController.registerToggleClass(element, attrs['toggleClass']);
            }
        };
    });
