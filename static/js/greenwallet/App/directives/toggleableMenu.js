/**
 * Created by PhpStorm.
 * User: Oleg Kudrenko <oleg.kudrenko@gmail.com>
 * Date: 7/28/14
 */
greenWalletApp
    .directive("toggleableMenu", ['$location', 'cordovaReady', 'backButtonHandler', 'vibration',
        function($location, cordovaReady, backButtonHandler, vibration) {
            return {
                restrict: 'A',
                controller: ['$scope', function($scope) {
                    var state = false;
                    if (window.cordova) {
                        var backHandler = function() {
                            $scope.toggle_set(false);
                        };
                        cordovaReady(function() {
                            document.addEventListener("menubutton", function() {
                                $scope.toggle_set(true);
                            });
                        })();
                    }
                    var toggleClasses = [];
                    this.registerToggleClass = function(element, cls) {
                        toggleClasses.push([element, cls]);
                    };
                    $scope.toggle_set = function(enable) {
                        if ($location.path() == '/') return;  // don't allow swiping menu on login page
                        vibration.vibrate(50);
                        if (state == enable) return;
                        state = enable;
                        for (var i = 0 ; i < toggleClasses.length; ++i) {
                            var tc = toggleClasses[i];
                            if (enable) {
                                tc[0].addClass(tc[1]);
                            } else {
                                tc[0].removeClass(tc[1]);
                            }
                        }
                        if (window.cordova) {
                            if (state) {
                                backButtonHandler.pushHandler(backHandler);
                            } else {
                                backButtonHandler.popHandler();
                            }
                        }
                    };
                }],
                link: function(scope, element, attrs) {
                    element.find('a').on('click', function() {
                        var a = angular.element(this);
                        scope.toggle_set(false);
                    });
                    scope.$watch(function() { return $location.path(); },
                        function(newValue, oldValue) {
                            var all_a = element.find('a');
                            for (var i = 0; i < all_a.length; i++) {
                                var a = angular.element(all_a[i]);
                                if (newValue.indexOf(a.parent().attr('path')) != -1 ||
                                    (a.parent().attr('path') == '/send' && newValue.indexOf('/uri/') != -1) ||
                                    (a.parent().attr('path') == '/send' && newValue.indexOf('/pay/') != -1) ||
                                    (a.parent().attr('path') == '/info' && newValue.indexOf('/redeem/') != -1)) {
                                    scope.subpage_title = a.text();
                                    vibration.vibrate(50);
                                    a.parent().addClass('selected');
                                } else {
                                    a.parent().removeClass('selected');
                                }
                            }
                        });
                }
            };
        }])
