var deps = [
    'ngRoute', 'ui.bootstrap', 'ja.qr',
    'greenWalletBaseApp',  // 'greenWalletDirectives', 'greenWalletControllers',
    'greenWalletSignupLogin',
    'greenWalletInfo', 'greenWalletTransactions', 'greenWalletSend',
    'greenWalletReceive', 'greenWalletSettings',
];

if (window.cordova) {
    deps.push('greenWalletNFCControllers');
    localStorage.hasWallet = true;  // enables redirect to wallet from front page (see js/greenaddress.js)
}
var greenWalletApp = angular.module('greenWalletApp', deps)
    .constant('branches', {
        REGULAR: 1,
        EXTERNAL: 2
    })
    .constant('social_types', {
        FACEBOOK: 0,
        EMAIL: 10,
        UNKNOWN: 100,
        PAYMENTREQUEST: 110
    })
    .config(['$routeProvider', '$provide', function config($routeProvider, $provide) {
        $routeProvider
            /*[SignupLoginController]*/
            .when('/', {
                templateUrl: BASE_URL+'/'+LANG+'/wallet/partials/signuplogin/base.html',
                controller: 'SignupLoginController'
            })
            .when('/trezor_login', {
                templateUrl: BASE_URL+'/'+LANG+'/wallet/partials/signuplogin/trezor.html',
                controller: 'SignupLoginController'
            })
            /*[SignupController]*/
            .when('/create_warning', {
                templateUrl: BASE_URL+'/'+LANG+'/wallet/partials/signup_0_warning.html',
                controller: 'SignupController'
            })
            .when('/create', {
                templateUrl: BASE_URL+'/'+LANG+'/wallet/partials/signup_1_init.html',
                controller: 'SignupController'
            })
            .when('/signup_pin', {
                templateUrl: BASE_URL+'/'+LANG+'/wallet/partials/signup_2_pin.html',
                controller: 'SignupController'
            })
            .when('/signup_oauth', {
                templateUrl: BASE_URL+'/'+LANG+'/wallet/partials/signup_3_oauth.html',
                controller: 'SignupController'
            })
            .when('/signup_2factor', {
                templateUrl: BASE_URL+'/'+LANG+'/wallet/partials/signup_4_2factor.html',
                controller: 'SignupController'
            })
            .when('/trezor_signup', {
                templateUrl: BASE_URL+'/'+LANG+'/wallet/partials/signup_2_trezor.html',
                controller: 'SignupController'
            })
            .when('/concurrent_login', {
                templateUrl: BASE_URL+'/'+LANG+'/wallet/partials/concurrent_login.html',
                controller: 'SignupController'
            })
            /*[InfoController]*/
            .when('/info', {
                templateUrl: BASE_URL+'/'+LANG+'/wallet/partials/wallet_info.html',
                controller: 'InfoController'
            })
            .when('/redeem/:enckey', {
                templateUrl: BASE_URL+'/'+LANG+'/wallet/partials/wallet_info.html',
                controller: 'InfoController'
            })
            /*[TransactionsController]*/
            .when('/transactions', {
                templateUrl: BASE_URL+'/'+LANG+'/wallet/partials/wallet_transactions.html',
                controller: 'TransactionsController'
            })
            /*[SendController]*/
            .when('/send', {
                templateUrl: BASE_URL+'/'+LANG+'/wallet/partials/wallet_send.html',
                controller: 'SendController'
            })
            .when('/send/:contact', {
                templateUrl: BASE_URL+'/'+LANG+'/wallet/partials/wallet_send.html',
                controller: 'SendController'
            })
            .when('/pay/:pay_receiver', {
                templateUrl: BASE_URL+'/'+LANG+'/wallet/partials/wallet_send.html',
                controller: 'SendController'
            })
            .when('/uri/', {
                templateUrl: BASE_URL+'/'+LANG+'/wallet/partials/wallet_send.html',
                controller: 'SendController'
            })
            /*[ReceiveController]*/
            .when('/receive', {
                templateUrl: BASE_URL+'/'+LANG+'/wallet/partials/wallet_receive.html',
                controller: 'ReceiveController'
            })
            /**Address Book**/
            .when('/address-book', {
                templateUrl: BASE_URL+'/'+LANG+'/wallet/partials/wallet_address_book.html'
                //controller: 'SettingsController'
            })
            .when('/address-book/name_:name', {
                templateUrl: BASE_URL+'/'+LANG+'/wallet/partials/wallet_address_book.html'
                //controller: 'SettingsController'
            })
            .when('/address-book/:page', {
                templateUrl: BASE_URL+'/'+LANG+'/wallet/partials/wallet_address_book.html'
                //controller: 'SettingsController'
            })
            /*[SettingsController]*/
            .when('/settings', {
                templateUrl: BASE_URL+'/'+LANG+'/wallet/partials/wallet_settings.html',
                controller: 'SettingsController'
            })
            /**browser_unsupported**/
            .when('/browser_unsupported', {
                templateUrl: BASE_URL+'/'+LANG+'/wallet/partials/browser_unsupported.html'
            });
    }])
    .run(['$rootScope', function($rootScope, $location) {
        $rootScope.$location = $location;
    }]);
