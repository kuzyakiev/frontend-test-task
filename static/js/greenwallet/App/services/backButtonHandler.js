/**
 * Created by PhpStorm.
 * User: Oleg Kudrenko <oleg.kudrenko@gmail.com>
 * Date: 7/28/14
 */
greenWalletApp
    .factory("backButtonHandler", function () {
        var backButtonHandlerService = {
            handlers: [],
            pushHandler: function(handler) {
                if (this.handlers.length) {
                    document.removeEventListener("backbutton", this.handlers[this.handlers.length-1]);
                }
                this.handlers.push(handler);
                document.addEventListener("backbutton", handler);
            },
            popHandler: function() {
                document.removeEventListener("backbutton", this.handlers.pop());
                if (this.handlers.length) {
                    document.addEventListener("backbutton", this.handlers[this.handlers.length-1]);
                }
            }
        };
        return backButtonHandlerService;
    });
