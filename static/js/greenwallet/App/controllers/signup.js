/**
 * Created by PhpStorm.
 * User: Oleg Kudrenko <oleg.kudrenko@gmail.com>
 * Date: 7/28/14
 */
greenWalletApp
    .controller('SignupController', ['$scope', '$injector', '$controller', function($scope, $injector, $controller) {
        $script(BASE_URL+'/static/js/signup.min.js', function() {
            // injector method takes an array of modules as the first argument
            // if you want your controller to be able to use components from
            // any of your other modules, make sure you include it together with 'ng'
            // Furthermore we need to pass on the $scope as it's unique to this controller
            $injector.invoke(SignupControllerAsync, this, {'$scope': $scope});
        });
    }]);