greenWalletSettings
    .controller('AutoLogoutController', ['$scope', 'notices', 'wallets', 'autotimeout', 'gaEvent', function AutoLogoutController($scope, notices, wallets, autotimeout, gaEvent) {

        if (!('appearance' in $scope.wallet)) return;

        $scope.timeoutstate = {timeout: false, altimeout: $scope.wallet.appearance.altimeout};

        autotimeout.registerObserverCallback(function() {
            $scope.mins = Math.floor(autotimeout.left / 1000 / 60);
            $scope.secs = Math.floor((autotimeout.left - ($scope.mins * 60 * 1000)) / 1000);

        });
        $scope.save_logout_timeout = function() {
            if ($scope.timeoutstate['altimeout'] === $scope.wallet.appearance.altimeout) return;
            if (!$scope.timeoutstate['timeout']) {
                $scope.timeoutstate['timeout'] = true;
            }
            wallets.updateAppearance($scope, 'altimeout', $scope.timeoutstate['altimeout']).then(function() {
                gaEvent('Wallet', "Timeoutset");
                //
                $scope.timeoutstate['timeout'] = false;
                $scope.wallet.appearance.altimeout = $scope.timeoutstate['altimeout'];
                autotimeout.start($scope.wallet.appearance.altimeout);

            }).catch(function(err) {
                gaEvent('Wallet', "TimeoutsetFailed");
                notices.makeNotice('error', err.desc);
                $scope.timeoutstate['timeout'] = false;
                $scope.altimeout = $scope.wallet.appearance.altimeout;
            });
        };
    }]);