greenWalletSettings
    .controller('TxLimitsController', ['$scope', 'gaEvent', '$modal', 'tx_sender', 'notices', 'wallets',
        function($scope, gaEvent, $modal, tx_sender, notices, wallets) {

            var formatAmountHumanReadable = function(units, is_fiat) {
                // for fiat, to fit the 'satoshi->BTC' conversion, the input value needs to be multiplied by 1M,
                // to get 1 fiat per 100 units
                var mul = {'BTC': 1, 'mBTC': 1000, 'µBTC': 1000000}[$scope.wallet.unit];
                var cur_mul = is_fiat ? 1000000 : mul;  // already satoshis for BTC
                var satoshi = new Bitcoin.BigInteger(units.toString()).multiply(Bitcoin.BigInteger.valueOf(cur_mul));
                return Bitcoin.Util.formatValue(satoshi.toString());
            };
            var formatAmountInteger = function(amount, is_fiat) {
                // for fiat, 'BTC->satoshi' parsed value needs to be divided by 1M, to get 100 units per 1 fiat
                var div = {'BTC': 1, 'mBTC': 1000, 'µBTC': 1000000}[$scope.wallet.unit];
                var cur_div = is_fiat ? 1000000 : div;
                var satoshi = Bitcoin.Util.parseValue(amount.toString()).divide(Bitcoin.BigInteger.valueOf(cur_div));
                return parseInt(satoshi.toString());
            }

            var modal;

            $scope.change_tx_limits = function() {
                gaEvent('Wallet', 'ChangeTxLimitsModal');
                $scope.limits_editor = {
                    currency: $scope.wallet.limits.is_fiat ? 'fiat' : 'BTC',
                    single_tx: formatAmountHumanReadable($scope.wallet.limits.per_tx, $scope.wallet.limits.is_fiat),
                    total: formatAmountHumanReadable($scope.wallet.limits.total, $scope.wallet.limits.is_fiat)
                };
                modal = $modal.open({
                    templateUrl: BASE_URL+'/'+LANG+'/wallet/partials/wallet_modal_tx_limits.html',
                    scope: $scope
                });
            };

            $scope.save_limits = function() {
                var is_fiat = $scope.limits_editor.currency == 'fiat';
                var data = {
                    is_fiat: is_fiat,
                    per_tx: formatAmountInteger($scope.limits_editor.single_tx, is_fiat),
                    total: formatAmountInteger($scope.limits_editor.total, is_fiat)
                };
                if (data.is_fiat != $scope.wallet.limits.is_fiat ||
                    data.per_tx > $scope.wallet.limits.per_tx ||
                    data.total > $scope.wallet.limits.total) {
                    var do_change = function() {
                        return wallets.get_two_factor_code($scope, 'change_tx_limits', data).then(function(twofac_data) {
                            return tx_sender.call('http://greenaddressit.com/login/change_settings', 'tx_limits', data, twofac_data);
                        });
                    }
                } else {
                    var do_change = function() {
                        return tx_sender.call('http://greenaddressit.com/login/change_settings', 'tx_limits', data);
                    }
                }
                $scope.limits_editor.saving = true;
                do_change().then(function() {
                    $scope.wallet.limits = data;
                    notices.makeNotice('success', gettext('Limits updated successfully'));
                    modal.close();
                }, function(err) {
                    notices.makeNotice('error', err.desc);
                }).finally(function() { $scope.limits_editor.saving = false; });
            };
        }]);