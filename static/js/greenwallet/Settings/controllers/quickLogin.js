greenWalletSettings
    .controller('QuickLoginController', ['$scope', 'tx_sender', 'notices', 'wallets', 'gaEvent', 'storage',
        function QuickLoginController($scope, tx_sender, notices, wallets, gaEvent, storage) {
            if (!wallets.requireWallet($scope, true)) return;   // dontredirect=true because one redirect in SettingsController is enough
            $scope.quicklogin = {enabled: false};
            tx_sender.call('http://greenaddressit.com/pin/get_devices').then(function(data) {
                angular.forEach(data, function(device) {
                    if (device.is_current) {
                        $scope.quicklogin.enabled = true;
                        $scope.quicklogin.device_ident = device.device_ident;
                    }
                });
                $scope.quicklogin.loaded = true;
                $scope.$watch('quicklogin.enabled', function(newValue, oldValue) {
                    if (newValue === oldValue) return
                    if (newValue && !$scope.quicklogin.started_unsetting_pin) {
                        if (!$scope.quicklogin.started_setting_pin) {
                            $scope.quicklogin.started_setting_pin = true;
                            $scope.quicklogin.enabled = false;  // not yet enabled
                        } else {
                            // finished setting pin
                            $scope.quicklogin.started_setting_pin = false;
                        }
                    } else if (!newValue && !$scope.quicklogin.started_setting_pin) {
                        if (!$scope.quicklogin.started_unsetting_pin) {
                            $scope.quicklogin.started_unsetting_pin = true;
                            $scope.quicklogin.enabled = true;  // not yet disabled
                            tx_sender.call('http://greenaddressit.com/pin/remove_pin_login',
                                $scope.quicklogin.device_ident).then(function(data) {
                                    gaEvent('Wallet', 'QuickLoginRemoved');
                                    $scope.quicklogin.enabled = false;
                                    $scope.quicklogin.device_ident = undefined;
                                    storage.remove('pin_ident');
                                    storage.remove('encrypted_seed');
                                    notices.makeNotice('success', gettext('PIN removed'));
                                }, function(err) {
                                    gaEvent('Wallet', 'QuickLoginRemoveFailed', err.desc);
                                    $scope.quicklogin.started_unsetting_pin = false;
                                    notices.makeNotice('error', err.desc);
                                });
                        } else {
                            // finished disabling pin
                            $scope.quicklogin.started_unsetting_pin = false;
                        }
                    }
                })
            });
            $scope.set_new_pin = function() {
                if (!$scope.quicklogin.new_pin) return;
                $scope.quicklogin.setting_pin = true;
                var success_message;
                var success = function(device_ident) {
                    $scope.quicklogin.setting_pin = false;
                    $scope.quicklogin.new_pin = '';
                    $scope.quicklogin.enabled = true;
                    if (device_ident) {
                        $scope.quicklogin.device_ident = device_ident;
                    }
                    notices.makeNotice('success', success_message);
                }, error = function(err) {
                    $scope.quicklogin.setting_pin = false;
                    $scope.quicklogin.started_setting_pin = false;
                    gaEvent('Wallet', 'PinError', err);
                    notices.makeNotice('error', err);
                };
                if ($scope.quicklogin.device_ident) {  // change the existing PIN
                    gaEvent('Wallet', 'PinChangeAttempt');
                    success_message = gettext('PIN changed');
                    tx_sender.change_pin($scope.quicklogin.new_pin).then(success, error);
                } else {  // create a brand new PIN
                    gaEvent('Wallet', 'NewPinSetAttempt');
                    success_message = gettext('PIN set');
                    wallets.create_pin($scope.quicklogin.new_pin, $scope).then(
                        success, error);
                }
            };
            $scope.remove_all_pin_logins = function() {
                $scope.quicklogin.started_unsetting_pin = true;
                tx_sender.call('http://greenaddressit.com/pin/remove_all_pin_logins').then(function() {
                    gaEvent('Wallet', 'AllPinLoginsRemoved');
                    $scope.quicklogin.enabled = false;
                    $scope.quicklogin.device_ident = undefined;
                    storage.remove('pin_ident');
                    storage.remove('encrypted_seed');
                    notices.makeNotice('success', gettext('All PINs removed'));
                }, function(err) {
                    gaEvent('Wallet', 'AllPinLoginsRemoveFailed', err.desc);
                    $scope.quicklogin.started_unsetting_pin = false;
                    notices.makeNotice('error', err.desc);
                });
            }
        }]);