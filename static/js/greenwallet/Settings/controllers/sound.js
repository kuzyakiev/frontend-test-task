greenWalletSettings
    .controller('SoundController', ['$scope', 'notices', 'wallets', 'gaEvent', function SoundController($scope, notices, wallets, gaEvent) {

        if (!('wallet' in $scope) || !('appearance' in $scope.wallet)) return;
        var soundstate = {sound: false};
        $scope.$watch('wallet.appearance.sound', function(newValue, oldValue) {
            if (newValue === oldValue) return;
            if (!soundstate['sound']) {
                soundstate['sound'] = true;
                if (!('wallet' in $scope) || !('appearance' in $scope.wallet) || !('sound' in $scope.wallet.appearance)) return;
                wallets.updateAppearance($scope, 'sound', newValue).then(function() {
                    gaEvent('Wallet', "Sound_"+(newValue?'Enabled':'Disabled'));
                    soundstate['sound'] = false;
                }).catch(function(err) {
                    gaEvent('Wallet', "Sound_"+(newValue?'Enable':'Disable')+'Failed', err.desc);
                    notices.makeNotice('error', err.desc);
                    soundstate['sound'] = false;
                });
            }
        });
    }]);