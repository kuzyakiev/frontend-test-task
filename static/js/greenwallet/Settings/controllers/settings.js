greenWalletSettings
    .controller('SettingsController', ['$scope', '$q', 'wallets', 'tx_sender', 'notices', '$modal', 'gaEvent', 'storage', '$location', '$timeout', 'bip38', 'mnemonics',
        function SettingsController($scope, $q, wallets, tx_sender, notices, $modal, gaEvent, storage, $location, $timeout, bip38, mnemonics) {
            if (!wallets.requireWallet($scope)) return;
            var exchanges = $scope.exchanges = {
                BITSTAMP: 'Bitstamp',
                LOCALBTC: 'LocalBitcoins',
                BTCAVG: 'BitcoinAverage'
            };
            var userfriendly_blocks = function(num) {
                return gettext("(about %s days: 1 day ≈ 144 blocks)").replace("%s", Math.round(num/144));
            };
            var settings = $scope.settings = {
                noLocalStorage: storage.noLocalStorage,
                unit: $scope.wallet.unit,
                pricing_source: $scope.wallet.fiat_currency + '|' + $scope.wallet.fiat_exchange,
                notifications: angular.copy($scope.wallet.appearance.notifications_settings || {}),
                language: LANG,
                updating_display_fiat: false,
                nlocktime: {
                    blocks: $scope.wallet.nlocktime_blocks,
                    blocks_new: $scope.wallet.nlocktime_blocks,
                    update: function() {
                        this.updating_nlocktime_blocks = true;
                        var that = this;
                        wallets.get_two_factor_code($scope, 'change_nlocktime', {'value': that.blocks_new}).then(function(twofac_data) {
                            return tx_sender.call('http://greenaddressit.com/login/set_nlocktime', that.blocks_new, twofac_data).then(function() {
                                $scope.wallet.nlocktime_blocks = that.blocks = that.blocks_new;
                                notices.makeNotice('success', gettext('nLockTime settings updated successfully'));
                            }, function(err) {
                                notices.makeNotice('error', err.desc);
                            });
                        }).finally(function() { that.updating_nlocktime_blocks = false; });
                    }
                },
                nfcmodal: function() {
                    gaEvent('Wallet', 'SettingsNfcModal');
                    mnemonics.validateMnemonic($scope.wallet.mnemonic).then(function(bytes) {
                        $scope.nfc_bytes = bytes;
                        $scope.nfc_mime = 'x-gait/mnc';
                        $modal.open({
                            templateUrl: BASE_URL+'/'+LANG+'/wallet/partials/signup_nfc_modal.html',
                            scope: $scope,
                            controller: 'NFCController'
                        });
                    });
                },
                nfcmodal_encrypted: function() {
                    gaEvent('Wallet', 'SettingsNfcModal');
                    mnemonics.validateMnemonic($scope.wallet.mnemonic).then(function(bytes) {
                        bip38.encrypt_mnemonic_modal($scope, bytes).then(function(mnemonic_encrypted) {
                            mnemonics.validateMnemonic(mnemonic_encrypted).then(function(bytes_encrypted) {
                                $scope.nfc_bytes = bytes_encrypted;
                                $scope.nfc_mime = 'x-gait/enc';
                                $modal.open({
                                    templateUrl: BASE_URL+'/'+LANG+'/wallet/partials/signup_nfc_modal.html',
                                    scope: $scope,
                                    controller: 'NFCController'
                                });
                            });
                        });
                    });
                },
                expiring_soon_modal: function() {
                    gaEvent('Wallet', 'ExpiringSoonModal');
                    tx_sender.call('http://greenaddressit.com/txs/upcoming_nlocktime').then(function(data) {
                        $scope.soon_nlocktimes = data;
                        $scope.soon_nlocktimes.estimate_days = function(nlocktime_at) {
                            var remaining_blocks = nlocktime_at - this.cur_block;
                            if (remaining_blocks <= 0) return gettext('Already expired');
                            else return gettext('in about %s days').replace('%s', Math.round(remaining_blocks/144));
                        };
                        $modal.open({
                            templateUrl: BASE_URL+'/'+LANG+'/wallet/partials/wallet_modal_expiring_soon.html',
                            scope: $scope
                        });
                    }, function(err) {
                        notices.makeNotice('error', err.desc);
                    });
                },
                send_nlocktime: function() {
                    var that = this;
                    that.sending_nlocktime = true;
                    gaEvent('Wallet', 'SendNlocktimeByEmail');
                    tx_sender.call('http://greenaddressit.com/txs/send_nlocktime').then(function(data) {
                        notices.makeNotice('success', gettext('Email sent'));
                    }, function(err) {
                        notices.makeNotice('error', err.desc);
                    }).finally(function() { that.sending_nlocktime = false; });
                }
            };
            $scope.settings.available_units = ['BTC', 'mBTC', 'µBTC'];
            tx_sender.call('http://greenaddressit.com/login/available_currencies').then(function(data) {
                $scope.settings.pricing_sources = [];
                for (var i = 0; i < data.all.length; i++) {
                    var currency = data.all[i];
                    for (var exchange in data.per_exchange) {
                        if (data.per_exchange[exchange].indexOf(currency) != -1) {
                            $scope.settings.pricing_sources.push({currency: currency, exchange: exchange});
                        }
                    }
                }
            });
            $scope.$watch('settings.nlocktime.blocks_new', function(newValue, oldValue) {
                settings.nlocktime.blocks_userfriendly = userfriendly_blocks(settings.nlocktime.blocks_new);
            });
            $scope.$watch('wallet.twofac.email_addr', function(newValue, oldValue) {
                settings.new_email = newValue;
            });
            if (!settings.currency) {
                $scope.$on('first_balance_updated', function() {
                    settings.pricing_source = $scope.wallet.fiat_currency + '|' + $scope.wallet.fiat_exchange;
                })
            }
            var ignoreLangChange = false;
            $scope.$watch('settings.language', function(newValue, oldValue) {
                if (newValue == oldValue) return;
                if (ignoreLangChange) { ignoreLangChange = false; return; }
                settings.language = oldValue;
                ignoreLangChange = true;  // don't ask for logout on change back to previous lang
                var is_chrome_app = window.chrome && chrome.storage;
                wallets.askForLogout($scope, gettext('You need to log out for language changes to be applied.')).then(function() {
                    if (is_chrome_app) {
                        storage.set('language', newValue);
                        chrome.runtime.sendMessage({changeLang: true, lang: newValue});
                    } else if (window.cordova) {
                        plugins.appPreferences.store(function() {
                            window.location.href = BASE_URL + '/' + newValue + '/' + 'index.html';
                        }, function(error) {
                            notices.makeNotice('error', gettext('Error changing language:') + ' ' + error);
                        }, 'language', newValue);
                    } else {
                        window.location.href = '/'+newValue+'/wallet';
                    }
                });
            });
            $scope.$watch('settings.pricing_source', function(newValue, oldValue) {
                var currency = newValue.split('|')[0];
                var exchange = newValue.split('|')[1];
                if (oldValue !== newValue && !settings.updating_pricing_source &&
                    (currency != $scope.wallet.fiat_currency || exchange != $scope.wallet.fiat_exchange)) {
                    // no idea why oldValue-on-error handling doesn't work without $timeout here
                    $timeout(function() { settings.pricing_source = oldValue; });
                    settings.updating_pricing_source = true;
                    var update = function() {
                        tx_sender.call('http://greenaddressit.com/login/set_pricing_source', currency, exchange).then(function() {
                            gaEvent('Wallet', 'PricingSourceChanged', newValue);
                            $scope.wallet.fiat_currency = currency;
                            $scope.wallet.fiat_exchange = exchange;
                            $scope.wallet.update_balance();
                            tx_sender.call("http://greenaddressit.com/login/get_spending_limits").then(function(data) {
                                // we reset limits if we change currency source while limits are fiat
                                $scope.wallet.limits.per_tx = data.per_tx;
                                $scope.wallet.limits.total = data.total;
                            });
                            settings.pricing_source = newValue;
                            settings.updating_pricing_source = false;
                        }).catch(function(err) {
                            settings.updating_pricing_source = false;
                            if (err.uri == "http://greenaddressit.com/error#exchangecurrencynotsupported") {
                                gaEvent('Wallet', 'CurrencyNotSupportedByExchange');
                                notices.makeNotice('error', gettext('{1} supports only the following currencies: {2}')
                                    .replace('{1}', exchanges[exchange])
                                    .replace('{2}', err.detail.supported));
                            } else {
                                gaEvent('Wallet', 'PricingSourceChangeFailed', err.desc);
                                notices.makeNotice('error', err.desc);
                            }
                        });
                    };
                    if ($scope.wallet.limits.is_fiat && parseInt($scope.wallet.limits.total)) {
                        $modal.open({
                            templateUrl: BASE_URL+'/'+LANG+'/wallet/partials/wallet_modal_tx_limits_fiat_warning.html'
                        }).result.then(update, function() { settings.updating_pricing_source = false; });
                    } else {
                        update();
                    }


                }
            });
            $scope.$watch('settings.currency', function(newValue, oldValue) {
                if (oldValue !== newValue && !settings.updating_currency && newValue != $scope.wallet.fiat_currency) {
                    settings.currency = oldValue;
                    settings.updating_currency = true;
                    tx_sender.call('http://greenaddressit.com/login/set_currency', newValue).then(function() {
                        gaEvent('Wallet', 'CurrencyChanged', newValue);
                        $scope.wallet.fiat_currency = newValue;
                        $scope.wallet.update_balance();
                        settings.currency = newValue;
                        settings.updating_currency = false;
                    }).catch(function(err) {
                        settings.updating_currency = false;
                        if (err.uri == "http://greenaddressit.com/error#exchangecurrencynotsupported") {
                            gaEvent('Wallet', 'CurrencyNotSupportedByExchange');
                            notices.makeNotice('error', gettext('{1} supports only the following currencies: {2}')
                                .replace('{1}', exchanges[settings.exchange])
                                .replace('{2}', err.detail.supported));
                        } else {
                            gaEvent('Wallet', 'CurrencyChangeFailed', err.desc);
                            notices.makeNotice('error', err.desc);
                        }
                    });
                }
            });
            $scope.$watch('settings.exchange', function(newValue, oldValue) {
                if (oldValue !== newValue && !settings.updating_exchange && newValue != $scope.wallet.fiat_exchange) {
                    settings.exchange = oldValue;
                    settings.updating_exchange = true;
                    tx_sender.call('http://greenaddressit.com/login/set_exchange', newValue).then(function() {
                        gaEvent('Wallet', 'ExchangeChanged', newValue);
                        $scope.wallet.fiat_exchange = newValue;
                        $scope.wallet.update_balance();
                        settings.exchange = newValue;
                        settings.updating_exchange = false;
                    }).catch(function(err) {
                        settings.updating_exchange = false;
                        if (err.uri == "http://greenaddressit.com/error#exchangecurrencynotsupported") {
                            gaEvent('Wallet', 'CurrencyNotSupportedByExchange');
                            notices.makeNotice('error', gettext('{1} supports only the following currencies: {2}')
                                .replace('{1}', exchanges[newValue])
                                .replace('{2}', err.detail.supported));
                        } else {
                            gaEvent('Wallet', 'ExchangeChangeFailed', err.desc);
                            notices.makeNotice('error', err.desc);
                        }
                    });
                }
            });
            var watchNotificationsEmail = function(inout, eventprefix) {
                $scope.$watch('settings.notifications.email_'+inout, function(newValue, oldValue) {
                    if (newValue === oldValue) return;
                    if (!settings['updating_ntf_email_'+inout] && newValue !==
                        ($scope.wallet.appearance.notifications_settings||{})['email_'+inout]) {
                        var notificationsNewValue = angular.copy(settings.notifications);
                        settings.notifications['email_'+inout] = oldValue;
                        settings['updating_ntf_email_'+inout] = true;
                        wallets.updateAppearance($scope, 'notifications_settings', notificationsNewValue).then(function() {
                            gaEvent('Wallet', eventprefix+(newValue?'Enabled':'Disabled'));
                            settings.notifications['email_'+inout] = newValue;
                            settings['updating_ntf_email_'+inout] = false;
                        }).catch(function(err) {
                            gaEvent('Wallet', eventprefix+(newValue?'Enable':'Disable')+'Failed', err.desc);
                            notices.makeNotice('error', err.desc);
                            settings['updating_ntf_email_'+inout] = false;
                        });
                    }
                });
            };
            watchNotificationsEmail('incoming', 'EmailIncomingNotifications');
            watchNotificationsEmail('outgoing', 'EmailOutgoingNotifications');
            $scope.$watch('settings.unit', function(newValue, oldValue) {
                if (oldValue !== newValue && !settings.updating_unit && newValue != $scope.wallet.appearance.unit) {
                    settings.unit = oldValue;
                    settings.updating_unit = true;
                    wallets.updateAppearance($scope, 'unit', newValue).then(function() {
                        gaEvent('Wallet', 'UnitChanged', newValue);
                        settings.unit = $scope.wallet.unit = newValue;
                        settings.updating_unit = false;
                    }).catch(function(err) {
                        gaEvent('Wallet', 'UnitChangeFailed', err.desc);
                        notices.makeNotice('error', err.desc);
                        settings.updating_unit = false;
                    });
                }
            });
            $scope.enable_link_handler = function() {
                try {
                    navigator.registerProtocolHandler('bitcoin', 'https://'+window.location.hostname+'/uri/?uri=%s', 'GreenAddress.It');
                    notices.makeNotice('success', gettext('Sent handler registration request'));
                } catch(e) {
                    notices.makeNotice('error', e.toString());
                }
            };
            $scope.show_encrypted_mnemonic = function() {
                gaEvent('Wallet', 'ShowEncryptedMnemonic');
                mnemonics.fromMnemonic($scope.wallet.mnemonic).then(function(data) {
                    bip38.encrypt_mnemonic_modal($scope, data).then(function(mnemonic_encrypted) {
                        $scope.mnemonic_encrypted = mnemonic_encrypted;
                        $modal.open({
                            templateUrl: BASE_URL+'/'+LANG+'/wallet/partials/wallet_modal_mnemonic.html',
                            scope: $scope
                        });
                    });
                });
            };
            $scope.show_mnemonic = function() {
                gaEvent('Wallet', 'ShowMnemonic');
                $scope.mnemonic_encrypted = undefined;
                $modal.open({
                    templateUrl: BASE_URL+'/'+LANG+'/wallet/partials/wallet_modal_mnemonic.html',
                    scope: $scope
                });
            };
            $scope.remove_account = function() {
                gaEvent('Wallet', 'RemoveAccountClicked');
                if ($scope.wallet.final_balance != 0) {
                    notices.makeNotice('error', gettext("Cannot remove an account with non-zero balance"))
                    return;
                }
                $modal.open({
                    templateUrl: BASE_URL+'/'+LANG+'/wallet/partials/wallet_modal_remove_account.html',
                }).result.then(function() {
                        wallets.get_two_factor_code($scope, 'remove_account', {}).then(function(twofac_data) {
                            return tx_sender.call('http://greenaddressit.com/login/remove_account', twofac_data).then(function() {
                                tx_sender.logout();
                                storage.remove('pin_ident');
                                storage.remove('encrypted_seed');
                                $location.path('/');
                            }).catch(function(err) {
                                notices.makeNotice('error', err.desc);
                                return $q.reject(err);
                            });
                        });
                    });
            };
            $scope.set_new_email = function() {
                if ($scope.wallet.twofac.email) {
                    notices.makeNotice('error', gettext("You can't change your email address while you have email 2FA enabled"));
                    return;
                }
                settings.setting_email = true;
                wallets.get_two_factor_code($scope, 'set_email', {'address': settings.new_email}).then(function(twofac_data) {
                    return tx_sender.call('http://greenaddressit.com/twofactor/set_email', settings.new_email, twofac_data).then(function() {
                        wallets.getTwoFacConfig($scope, true);  // refresh twofac config
                    }).catch(function(err) {
                        notices.makeNotice('error', err.desc);
                        return $q.reject(err);
                    });
                }).finally(function() { settings.setting_email = false; });
            };
            $scope.confirm_email = function() {
                tx_sender.call('http://greenaddressit.com/twofactor/activate_email', settings.email_confirmation_code).then(function() {
                    wallets.getTwoFacConfig($scope, true);  // refresh twofac config
                    notices.makeNotice('success', gettext('Email confirmed'))
                }).catch(function(err) {
                    notices.makeNotice('error', err.desc);
                });
            };
        }]);