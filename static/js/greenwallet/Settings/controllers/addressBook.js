greenWalletSettings
    .controller('AddressBookController', ['$scope', 'tx_sender', 'notices', 'focus', 'wallets', '$location', 'gaEvent', '$rootScope', '$routeParams', 'addressbook',
        function AddressBookController($scope, tx_sender, notices, focus, wallets, $location, gaEvent, $rootScope, $routeParams, addressbook) {
            // dontredirect=false here because address book is now outside settings,
            // though it's also used from inside SendController, hence the $location.url() check
            if (!wallets.requireWallet($scope, $location.url().indexOf('/address-book') != -0)) return;
            $routeParams.page = $routeParams.page || 1;
            $routeParams.page = parseInt($routeParams.page);
            $scope.route = $routeParams;
            $scope.addressbook = addressbook;
            addressbook.load($scope, $routeParams);
            $scope.add = function() {
                gaEvent('Wallet', 'AddressBookNewItemStarted');
                addressbook.new_item = {name: '', address: '', type: 'address'};
                focus('addrbook_new_item');
            };
            $scope.delete = function(address) {
                gaEvent('Wallet', 'AddressBookDeleteItem');
                tx_sender.call('http://greenaddressit.com/addressbook/delete_entry', address).then(function() {
                    var filtered_items = [];
                    angular.forEach(addressbook.items, function(value) {
                        if (value.address != address) {
                            filtered_items.push(value);
                        }
                    });
                    addressbook.items = filtered_items;
                    addressbook.init_partitions();
                    addressbook.num_pages = Math.ceil(addressbook.items.length / 20);
                    addressbook.populate_csv();
                    $scope.wallet.refresh_transactions();  // update sender/recipient names
                });
            };
            $scope.rename = function(address, name) {
                tx_sender.call('http://greenaddressit.com/addressbook/edit_entry', address, name, 0).then(function(data) {
                    gaEvent('Wallet', 'AddressBookItemRenamed');
                    angular.forEach(addressbook.items, function(value) {
                        if (value.address == address) {
                            value.renaming = false;
                        }
                    });
                    $scope.wallet.refresh_transactions();  // update sender/recipient names
                }, function(err) {
                    gaEvent('Wallet', 'AddressBookItemRenameFailed', err.desc);
                    notices.makeNotice('error', 'Error renaming item: ' + err.desc);
                });
            };
            $scope.start_rename = function(item) {
                gaEvent('Wallet', 'AddressBookRenameItemStarted');
                item.renaming = true;
                focus('addrbook_rename_' + item.address);
            };
            $scope.save = function() {
                var item = addressbook.new_item;
                if (item.address.indexOf('@') != -1) {
                    item.type = 'email';
                }
                tx_sender.call('http://greenaddressit.com/addressbook/add_entry',
                    item.address, item.name, 0).then(function(data) {
                        if (!data) {
                            gaEvent('Wallet', 'AddressBookItemAddFailed', '!data');
                            notices.makeNotice('error', 'Error saving item');
                            return;
                        } else {
                            gaEvent('Wallet', 'AddressBookItemAdded');

                            addressbook.new_item = undefined;
                            notices.makeNotice('success', gettext('New item saved'));
                            $scope.wallet.refresh_transactions();  // update sender/recipient names
                            // go to first page - it should refresh the view:
                            $location.path('/address-book/name_'+encodeURIComponent(item.name));
                        }
                    }, function(err) {
                        gaEvent('Wallet', 'AddressBookItemAddFailed', err.desc);
                        notices.makeNotice('error', gettext('Error saving item: ') + err.desc);
                    });
            };
            $scope.send_url = function(contact) {
                return '#/send/' + Bitcoin.base58.encode(
                    Bitcoin.convert.hexToBytes(Bitcoin.CryptoJS.enc.Utf8.parse(
                        JSON.stringify(contact)).toString()));
            };
        }]);