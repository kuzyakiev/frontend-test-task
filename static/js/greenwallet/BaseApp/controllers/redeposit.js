/**
 * Created by PhpStorm.
 * User: Oleg Kudrenko <oleg.kudrenko@gmail.com>
 * Date: 7/28/14
 */
greenWalletBaseApp
    .controller('RedepositController', ['$scope', 'tx_sender', 'wallets', 'notices', '$q',
        function RedepositController($scope, tx_sender, wallets, notices, $q) {
            $scope.redeposit_estimated_fees = {
                single_tx: 10000 * Math.ceil((300 + $scope.wallet.expired_deposits.length * 180) / 1000),
                multiple_tx: 10000 * $scope.wallet.expired_deposits.length
            };
            var redeposit = function (txos, twofac_data) {
                var deferred = $q.defer();
                var txos_in = [];
                for (var i = 0; i < txos.length; ++i) {
                    txos_in.push([txos[i].txhash, txos[i].out_n]);
                }
                tx_sender.call("http://greenaddressit.com/vault/prepare_redeposit", txos_in).then(function (data) {
                    if (twofac_data) {
                        var scope = undefined;
                    } else {
                        var scope = $scope;  // it asks for two fac if scope is provided
                    }
                    wallets.sign_and_send_tx(scope, data, false, twofac_data).then(function () {
                        deferred.resolve();
                    }, function (err) {
                        deferred.reject(err);
                    });
                }, function (error) {
                    deferred.reject(error.desc);
                });
                return deferred.promise;
            };
            $scope.redeposit_single_tx = function () {
                redeposit($scope.wallet.expired_deposits).then(function () {
                    notices.makeNotice('success', gettext('Re-depositing successful!'));
                }, function (err) {
                    notices.makeNotice('error', err);
                });
            };
            $scope.redeposit_multiple_tx = function () {
                return tx_sender.call("http://greenaddressit.com/vault/prepare_redeposit",
                    [[$scope.wallet.expired_deposits[0].txhash, $scope.wallet.expired_deposits[0].out_n]]).then(function () {
                        // prepare one to set appropriate tx data for 2FA
                        return wallets.get_two_factor_code($scope, 'send_tx').then(function (twofac_data) {
                            var promise = $q.when();
                            for (var i = 0; i < $scope.wallet.expired_deposits.length; ++i) {
                                (function (i) {
                                    promise = promise.then(function () {
                                        return redeposit([$scope.wallet.expired_deposits[i]], twofac_data);
                                    }, function (err) {
                                        return $q.reject(err);
                                    });
                                })(i);
                            }
                            promise.then(function () {
                                notices.makeNotice('success', gettext('Re-depositing successful!'));
                            }, function (error) {
                                notices.makeNotice('error', error);
                            });
                            return promise;
                        });
                    }, function (error) {
                        notices.makeNotice('error', error);
                    })

            };
        }]);