/**
 * Created by PhpStorm.
 * User: Oleg Kudrenko <oleg.kudrenko@gmail.com>
 * Date: 7/28/14
 */
greenWalletBaseApp
    .controller('UrlQRController', ['$scope', 'url', function UrlQRController($scope, url) {
        $scope.url = url;
    }]);