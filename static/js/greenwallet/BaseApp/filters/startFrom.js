/**
 * Created by PhpStorm.
 * User: Oleg Kudrenko <oleg.kudrenko@gmail.com>
 * Date: 7/28/14
 */
greenWalletBaseApp
    .filter('startFrom', function() {
        return function(input, start) {
            if (!input) return input;
            start = +start; //parse to int
            return input.slice(start);
        };
    });