/**
 * Created by PhpStorm.
 * User: Oleg Kudrenko <oleg.kudrenko@gmail.com>
 * Date: 7/28/14
 */
greenWalletBaseApp
    .filter('format_btc_nounit', ['btc_formatter', function(btc_formatter) {
        return function format_btc_nounit(satoshis, unit) {
            return btc_formatter(satoshis, unit);
        };
    }]);