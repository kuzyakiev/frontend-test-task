/**
 * Created by PhpStorm.
 * User: Oleg Kudrenko <oleg.kudrenko@gmail.com>
 * Date: 7/28/14
 */
greenWalletBaseApp
    .directive('masknput', function() {
        // should be used with class="pin" or otherwise something that sets webkitTextSecurity
        return {
            link: function(scope, element) {
                var style = window.getComputedStyle(element[0]);
                if(style.webkitTextSecurity) {
                    // do nothing
                } else {
                    // Firefox doesn't support -webkit-text-security
                    element[0].setAttribute("type", "password");
                }
            }
        };
    });