/**
 * Created by PhpStorm.
 * User: Oleg Kudrenko <oleg.kudrenko@gmail.com>
 * Date: 7/28/14
 */
greenWalletBaseApp
    .directive('focusOn', ['$timeout', function($timeout) {
        return function(scope, elem, attr) {
            scope.$on('focusOn', function(e, name) {
                if(name === attr.focusOn) {
                    $timeout(function() { elem[0].focus(); });
                }
            });
        };
    }]);
