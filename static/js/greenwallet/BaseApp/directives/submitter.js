/**
 * Created by PhpStorm.
 * User: Oleg Kudrenko <oleg.kudrenko@gmail.com>
 * Date: 7/28/14
 */
greenWalletBaseApp
    .directive('submitter', function() {
        return {
            require: '^submittable',
            link: function(scope, element, attrs, submittableCtrl) { submittableCtrl.setSubmitter(element); }
        };
    });
