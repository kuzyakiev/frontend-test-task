/**
 * Created by PhpStorm.
 * User: Oleg Kudrenko <oleg.kudrenko@gmail.com>
 * Date: 7/28/14
 */
greenWalletBaseApp
    .directive('gaClickNoTouch', ['$parse', function($parse) {
        return {
            compile: function($element, attr) {
                var fn = $parse(attr['gaClickNoTouch']);
                return function(scope, element, attr) {
                    element.on('click', function(event) {
                        scope.$apply(function() {
                            fn(scope, {$event:event});
                        });
                    });
                };
            }
        };
    }]);