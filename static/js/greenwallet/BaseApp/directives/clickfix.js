/**
 * Created by PhpStorm.
 * User: Oleg Kudrenko <oleg.kudrenko@gmail.com>
 * Date: 7/28/14
 */
greenWalletBaseApp
    .directive('clickfix', function() {
        return {
            link: function(scope, element, attrs) {
                element.on('touchstart', function(event) {
                    event.stopPropagation();
                    var clickEvent = document.createEvent('MouseEvent');
                    clickEvent.initEvent('click', true, true);
                    element[0].dispatchEvent(clickEvent);
                });
            }
        };
    });
