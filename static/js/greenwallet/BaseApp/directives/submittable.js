/**
 * Created by PhpStorm.
 * User: Oleg Kudrenko <oleg.kudrenko@gmail.com>
 * Date: 7/28/14
 */
greenWalletBaseApp
    .directive('submittable', function() {
        return {
            scope: true,
            controller: ['$scope', '$timeout', function SubmittableController($scope, $timeout) {
                var submitter;
                this.setSubmitter = function(newSubmitter) { submitter = newSubmitter; }
                $scope.submit_me = function() { $timeout(function(){
                    var clickEvent = document.createEvent('MouseEvent');
                    clickEvent.initEvent('click', true, true);
                    submitter[0].dispatchEvent(clickEvent);
                }); }
            }],
            link: function($scope, elem) {
                elem.find('input').on('invalid', function() {
                    $scope.safeApply(function() { if($scope.state) $scope.state.error = true; });
                });
            }
        };
    });