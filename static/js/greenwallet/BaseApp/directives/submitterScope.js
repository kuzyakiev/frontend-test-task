/**
 * Created by PhpStorm.
 * User: Oleg Kudrenko <oleg.kudrenko@gmail.com>
 * Date: 7/28/14
 */
greenWalletBaseApp
    .directive('submitterScope', function() {
        // used by getTemplate() in settings/directives.js
        return {
            link: function(scope, element, attrs) { scope.submittableCtrl.setSubmitter(element); }
        };
    });
