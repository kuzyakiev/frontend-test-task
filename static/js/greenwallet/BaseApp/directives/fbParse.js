/**
 * Created by PhpStorm.
 * User: Oleg Kudrenko <oleg.kudrenko@gmail.com>
 * Date: 7/28/14
 */
greenWalletBaseApp
    .directive('fbParse', ['facebook', function(facebook) {
        return function(scope, elem, attr) {
            FB.XFBML.parse(elem[0]);
        };
    }]);
