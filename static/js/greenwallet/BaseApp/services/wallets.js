greenWalletBaseApp.factory('wallets', ['$q', '$rootScope', 'tx_sender', '$location', 'notices', '$modal', 'focus', 'crypto', 'gaEvent', 'storage', 'mnemonics', 'addressbook', 'autotimeout', 'social_types', 'sound', 'vibration',
    function($q, $rootScope, tx_sender, $location, notices, $modal, focus, crypto, gaEvent, storage, mnemonics, addressbook, autotimeout, social_types, sound, vibration) {
        var walletsService = {};
        var handle_double_login = function(retry_fun) {
            return $modal.open({
                templateUrl: BASE_URL+'/'+LANG+'/wallet/partials/wallet_modal_logout_other_session.html'
            }).result.then(function() {
                    return retry_fun();
                });
        };
        walletsService.requireWallet = function($scope, dontredirect) {
            if (!$scope.wallet.hdwallet && !$scope.wallet.trezor_dev) {
                if (!dontredirect) {
                    var location = '/?redir=' + $location.path();
                    var search = '';
                    for (var key in $location.search()) {
                        if (i > 0) search += '&';
                        search += key + '=' + encodeURIComponent($location.search()[key]);
                    }
                    if (search) {
                        location += encodeURIComponent('?' + search);
                    }
                    $location.url(location);
                    $scope.processWalletVars();  // update payment values with redir value
                }
                return false;
            }
            return true;
        };
        walletsService.updateAppearance = function($scope, key, value) {
            var oldValue = $scope.wallet.appearance[key];
            $scope.wallet.appearance[key] = value;
            return tx_sender.call('http://greenaddressit.com/login/set_appearance', JSON.stringify($scope.wallet.appearance)).catch(function(e) {
                $scope.wallet.appearance[key] = oldValue;
                return $q.reject(e);
            });
        };
        var openInitialPage = function(wallet) {
            if ($location.search().redir) {
                $location.url($location.search().redir);
            } else if (window.IS_MOBILE || wallet.send_to_receiving_id || wallet.send_to_payment_request) {
                $location.path('/send');
            } else {
                $location.url('/info');
            }
        };
        walletsService._login = function($scope, hdwallet, mnemonic, signup, logout, path_seed, path) {
            var promise = tx_sender.login(logout), that = this;
            return promise.then(function(data) {
                if (data) {
                    if (window.disableEuCookieComplianceBanner) {
                        disableEuCookieComplianceBanner();
                    }
                    tx_sender.wallet = $scope.wallet;
                    $scope.wallet.hdwallet = hdwallet;
                    $scope.wallet.trezor_dev = tx_sender.trezor_dev;
                    $scope.wallet.mnemonic = mnemonic;
                    if (data.last_login) {
                        $scope.wallet.last_login = data.last_login;
                    }
                    try {
                        $scope.wallet.appearance = JSON.parse(data.appearance);
                        if ($scope.wallet.appearance.constructor !== Object) $scope.wallet.appearance = {};
                    } catch(e) {
                        $scope.wallet.appearance = {};
                    }
                    if (!('sound' in $scope.wallet.appearance)) {
                        $scope.wallet.appearance.sound = true;
                    }
                    if (!('vibrate' in $scope.wallet.appearance)) {
                        $scope.wallet.appearance.vibrate = true;
                    }
                    vibration.state = $scope.wallet.appearance.vibrate;
                    if (!('altimeout' in $scope.wallet.appearance)) {
                        $scope.wallet.appearance.altimeout = 20;
                    }
                    notices.makeNotice('success', gettext('Logged in!'));
                    sound.play(BASE_URL + "/static/sound/coinreceived.mp3", $scope);
                    autotimeout.start($scope.wallet.appearance.altimeout);
                    $scope.wallet.privacy = data.privacy;
                    $scope.wallet.limits = data.limits;
                    $scope.wallet.unit = $scope.wallet.appearance.unit || 'mBTC';
                    $scope.wallet.cache_password = data.cache_password;
                    $scope.wallet.fiat_exchange = data.exchange;
                    $scope.wallet.receiving_id = data.receiving_id;
                    $scope.wallet.expired_deposits = data.expired_deposits;
                    $scope.wallet.nlocktime_blocks = data.nlocktime_blocks;
                    if (path) {
                        $scope.wallet.gait_path = path;
                    } else {
                        $scope.wallet.gait_path_seed = path_seed;
                        $scope.wallet.gait_path = mnemonics.seedToPath(path_seed);
                    }
                    if (data.gait_path !== $scope.wallet.gait_path) {
                        tx_sender.call('http://greenaddressit.com/login/set_gait_path', $scope.wallet.gait_path).catch(function(err) {
                            if (err.uri != 'http://api.wamp.ws/error#NoSuchRPCEndpoint') {
                                notices.makeNotice('error', 'Please contact support (reference "sgp_error ' + err.desc + '")');
                            } else {
                                $scope.wallet.old_server = true;
                            }
                        });
                    }
                    if (!signup) {  // don't change URL on initial login in signup
                        openInitialPage($scope.wallet);
                    }
                    $rootScope.$broadcast('login');
                } else if (!signup) {  // signup has its own error handling
                    notices.makeNotice('error', gettext('Login failed'));
                }
                return data;
            }, function(err) {
                if (err.uri == 'http://greenaddressit.com/error#doublelogin') {
                    return handle_double_login(function() {
                        return that.login($scope, hdwallet, mnemonic, signup, true, path_seed);
                    });
                } else {
                    notices.makeNotice('error', gettext('Login failed') + ': ' + err.desc);
                    return $q.reject(err);
                }
            });
        };
        walletsService.login = function($scope, hdwallet, mnemonic, signup, logout, path_seed) {
            tx_sender.hdwallet = hdwallet;
            return this._login($scope, hdwallet, mnemonic, signup, logout, path_seed);
        };
        walletsService.login_trezor = function($scope, trezor_dev, path, signup, logout) {
            tx_sender.trezor_dev = trezor_dev;
            return this._login($scope, undefined, undefined, signup, logout, undefined, path, trezor_dev);
        };
        walletsService.loginWatchOnly = function($scope, token_type, token, logout) {
            var promise = tx_sender.loginWatchOnly(token_type, token, logout), that = this;
            promise = promise.then(function(json) {
                if (window.disableEuCookieComplianceBanner) {
                    disableEuCookieComplianceBanner();
                }
                var data = JSON.parse(json);
                tx_sender.wallet = $scope.wallet;
                var hdwallet = new Bitcoin.HDWallet();
                hdwallet.network = cur_net;
                hdwallet.pub = new Bitcoin.ECPubKey(Bitcoin.convert.hexToBytes(data.public_key));
                hdwallet.chaincode = Bitcoin.convert.hexToBytes(data.chain_code);
                $scope.wallet.hdwallet = hdwallet;
                try {
                    $scope.wallet.appearance = JSON.parse(data.appearance);
                    if ($scope.wallet.appearance.constructor !== Object) $scope.wallet.appearance = {};
                } catch(e) {
                    $scope.wallet.appearance = {};
                }
                if (!('sound' in $scope.wallet.appearance)) {
                    $scope.wallet.appearance.sound = true;
                }
                if (!('vibrate' in $scope.wallet.appearance)) {
                    $scope.wallet.appearance.vibrate = true;
                }
                if (!('altimeout' in $scope.wallet.appearance)) {
                    $scope.wallet.appearance.altimeout = 20;
                }

                autotimeout.start($scope.wallet.appearance.altimeout);
                $scope.wallet.unit = $scope.wallet.appearance.unit || 'mBTC';

                $scope.wallet.cache_password = data.cache_password;
                $scope.wallet.fiat_exchange = data.exchange;
                $scope.wallet.receiving_id = data.receiving_id;
                $location.url('/info/');
                $rootScope.$broadcast('login');
            }, function(err) {
                if (err.uri == 'http://greenaddressit.com/error#doublelogin') {
                    return handle_double_login(function() {
                        return that.loginWatchOnly($scope, token_type, token, true);
                    });
                } else {
                    return $q.reject(err);
                }
            });
            return promise;
        };
        walletsService.getTransactions = function($scope, notifydata) {
            var transactions_key = $scope.wallet.receiving_id + 'transactions'
            var deferreds = [addressbook.load($scope), storage.get(transactions_key)];
            return $q.all(deferreds).then(function(results) {
                var cache = results[1];
                try {
                    cache = JSON.parse(cache) || {items: []};
                } catch(e) {
                    cache = {items: []};
                }
                if (cache.last_txhash) {
                    return crypto.decrypt(cache.items, $scope.wallet.cache_password).then(function(decrypted) {
                        cache.items = JSON.parse(decrypted);
                        return walletsService._getTransactions($scope, cache, notifydata);
                    });
                } else cache.items = [];
                return walletsService._getTransactions($scope, cache, notifydata);
            });
        };
        walletsService._getTransactions = function($scope, cache, notifydata) {
            var transactions_key = $scope.wallet.receiving_id + 'transactions';
            var d = $q.defer();
            $rootScope.is_loading += 1;
            var unclaimed = [];
            for (var i = 0; i < cache.items.length; i++) {
                var item = cache.items[i];
                if (item.unclaimed) {
                    unclaimed.push(item.txhash);
                }
            }
            tx_sender.call('http://greenaddressit.com/txs/get_list', cache.last_txhash, unclaimed).then(function(data) {
                var retval = [];
                var output_values = {};
                var any_unconfirmed_seen = false;
                // prepend cache to the returned list
                for (var i = cache.items.length - 1; i >= 0; i--) {
                    var item = cache.items[i];
                    if (data.unclaimed[item.txhash]) {
                        // replace unclaimed in txcache
                        item = cache.items[i] = data.unclaimed[item.txhash];
                    }
                    data.list.unshift(cache.items[i]);
                }
                for (var i = 0; i < data.list.length; i++) {
                    var tx = data.list[i], inputs = [], outputs = [];
                    var num_confirmations = data.cur_block - tx.block_height + 1;
                    if (i >= cache.items.length && tx.block_height && num_confirmations >= 6 &&
                        // Don't store in cache if there are 'holes' between confirmed transactions
                        // which can be caused by some older transactions getting confirmed later than
                        // newer ones. Storing such newer txs and marking last_txhash can cause those
                        // older txs to be missing from the list, being unconfirmed and not added to cache.
                        !any_unconfirmed_seen) {
                        // store confirmed txs in cache
                        cache.items.push(tx);
                        cache.last_txhash = tx.txhash;
                    }
                    any_unconfirmed_seen = num_confirmations < 6;

                    var value = new Bitcoin.BigInteger('0'),
                        in_val = new Bitcoin.BigInteger('0'), out_val = new Bitcoin.BigInteger('0'),
                        redeemable_value = new Bitcoin.BigInteger('0'), sent_back_from, redeemable_unspent = false,
                        pubkey_pointer, sent_back = false, from_me = false;
                    var negative = false, positive = false, unclaimed = false, external_social = false;
                    for (var j = 0; j < tx.eps.length; j++) {
                        var ep = tx.eps[j];
                        if (ep.is_relevant && !ep.is_credit) from_me = true;
                    }
                    for (var j = 0; j < tx.eps.length; j++) {
                        var ep = tx.eps[j];
                        if (ep.is_relevant) {
                            if (ep.is_credit) {
                                var bytes = Bitcoin.base58.decode(ep.ad);
                                var version = bytes[0];
                                var _external_social = version != Bitcoin.network[cur_net].p2shVersion;
                                external_social = external_social || _external_social;

                                if (ep.social_destination && external_social) {
                                    pubkey_pointer = ep.pubkey_pointer;
                                    if (!from_me) {
                                        redeemable_value = redeemable_value.add(new Bitcoin.BigInteger(ep.value));
                                        sent_back_from = ep.social_destination;
                                        redeemable_unspent = redeemable_unspent || !ep.is_spent;
                                    }
                                } else {
                                    value = value.add(new Bitcoin.BigInteger(ep.value));
                                    ep.nlocktime = true;
                                }
                            }
                            else {
                                value = value.subtract(new Bitcoin.BigInteger(ep.value));
                            }
                        }
                        if (ep.is_credit) {
                            outputs.push(ep);
                            out_val = out_val.add(new Bitcoin.BigInteger(ep.value));
                            output_values[[tx.txhash, ep.pt_idx]] = new Bitcoin.BigInteger(ep.value);
                        } else { inputs.push(ep); in_val = in_val.add(new Bitcoin.BigInteger(ep.value)); }
                    }
                    if (value.compareTo(new Bitcoin.BigInteger('0')) > 0 || redeemable_value.compareTo(new Bitcoin.BigInteger('0')) > 0) {
                        if (notifydata && (tx.txhash == notifydata.txhash)) {
                            notices.makeNotice('success', gettext('Bitcoin transaction received!'));
                            sound.play(BASE_URL + "/static/sound/coinreceived.mp3", $scope);
                        }
                        positive = true;
                        if (redeemable_value.compareTo(new Bitcoin.BigInteger('0')) > 0) {
                            var description = gettext('Back from ') + sent_back_from;
                        } else {
                            var description = gettext('From ');
                            var addresses = [];
                            for (var j = 0; j < tx.eps.length; j++) {
                                var ep = tx.eps[j];
                                if (!ep.is_credit && !ep.is_relevant) {
                                    if (ep.social_source) {
                                        if (addresses.indexOf(ep.social_source) == -1) {
                                            addresses.push(ep.social_source);
                                        }
                                    } else {
                                        var ad = addressbook.reverse[ep.ad] || ep.ad;
                                        if (addresses.indexOf(ad) == -1) {
                                            addresses.push(ad);
                                        }
                                    }
                                }
                            }
                            description += addresses.length ? addresses[0] : '';
                            if (addresses.length > 1) {
                                description += ', ...';
                            }
                        }
                    } else {
                        negative = value.compareTo(new Bitcoin.BigInteger('0')) < 0;
                        var addresses = [];
                        var description = gettext('To ');
                        for (var j = 0; j < tx.eps.length; j++) {
                            var ep = tx.eps[j];
                            if (ep.is_credit && (!ep.is_relevant || ep.social_destination)) {
                                if (ep.social_destination && ep.social_destination_type != social_types.PAYMENTREQUEST) {
                                    pubkey_pointer = ep.pubkey_pointer;
                                    var bytes = Bitcoin.base58.decode(ep.ad);
                                    var version = bytes[0];
                                    var _external_social = version != Bitcoin.network[cur_net].p2shVersion;
                                    external_social = external_social || _external_social;
                                    if (!ep.is_spent && ep.is_relevant) {
                                        unclaimed = true;
                                        addresses.push(ep.social_destination);
                                    } else if (!ep.is_relevant && external_social) {
                                        sent_back = true;
                                        addresses.push(ep.ad);
                                    } else {
                                        addresses.push(ep.social_destination);
                                    }
                                } else if (ep.social_destination && ep.social_destination_type == social_types.PAYMENTREQUEST) {
                                    if (addresses.indexOf(ep.social_destination) == -1) {
                                        addresses.push(ep.social_destination);
                                    }
                                } else {
                                    var ad = addressbook.reverse[ep.ad] || ep.ad;
                                    addresses.push(ad);
                                }
                            }
                        }

                        if(sent_back) {
                            description = gettext('Sent back to ')
                        }
                        if (!addresses.length) {
                            description = gettext('Re-deposited');
                        } else {
                            description += addresses.join(', ');
                        }
                    }
                    // prepend zeroes for sorting
                    var value_sort = new Bitcoin.BigInteger(Math.pow(10, 19).toString()).add(value).toString();
                    while (value_sort.length < 20) value_sort = '0' + value_sort;
                    retval.unshift({ts: new Date(tx.created_at.replace(' ', 'T')), txhash: tx.txhash, memo: tx.memo,
                        value_sort: value_sort, value: value, instant: tx.instant,
                        value_fiat: data.fiat_value ? value * data.fiat_value / Math.pow(10, 8) : undefined,
                        redeemable_value: redeemable_value, negative: negative, positive: positive,
                        description: description, external_social: external_social, unclaimed: unclaimed,
                        pubkey_pointer: pubkey_pointer, inputs: inputs, outputs: outputs,
                        fee: in_val.subtract(out_val).toString(),
                        nonzero: value.compareTo(new Bitcoin.BigInteger('0')) != 0,
                        redeemable: redeemable_value.compareTo(new Bitcoin.BigInteger('0')) > 0,
                        redeemable_unspent: redeemable_unspent,
                        sent_back: sent_back, block_height: tx.block_height,
                        confirmations: tx.block_height ? data.cur_block - tx.block_height + 1: 0,
                        has_payment_request: tx.has_payment_request});
                    // tx.unclaimed is later used for cache updating
                    tx.unclaimed = retval[0].unclaimed || (retval[0].redeemable && retval[0].redeemable_unspent);
                }
                crypto.encrypt(JSON.stringify(cache.items), $scope.wallet.cache_password).then(function(encrypted) {
                    cache.items = encrypted;
                    storage.set(transactions_key, JSON.stringify(cache));
                });

                d.resolve({fiat_currency: data.fiat_currency, list: retval,
                    populate_csv: function() {
                        var csv_list = [gettext('Time,Description,satoshis,')+this.fiat_currency];
                        for (var i = 0; i < Math.min(this.limit, this.list.length); i++) {
                            var item = this.list[i];
                            csv_list.push(item.ts + ',' + item.description.replace(',', '\'') + ',' + item.value + ',' + item.value_fiat);
                        }
                        this.csv = 'data:text/csv;charset=utf-8,' + encodeURIComponent(csv_list.join('\n'));
                    },
                    output_values: output_values});
            }, function(err) {
                notices.makeNotice('error', err.desc);
            }).finally(function() { $rootScope.is_loading -= 1; });
            return d.promise
        };
        var _sign_and_send_tx = function($scope, data, priv_der, twofactor, notify) {
            var d = $q.defer();
            var tx = Bitcoin.Transaction.deserialize(data.tx);
            var signatures = [];
            for (var i = 0; i < tx.ins.length; ++i) {
                if (data.prev_outputs[i].privkey) {
                    var key = data.prev_outputs[i].privkey;
                } else {
                    var key = tx_sender.hdwallet;
                    if (priv_der) {
                        key = key.derivePrivate(data.prev_outputs[i].branch);
                        key = key.derivePrivate(data.prev_outputs[i].pointer);
                    } else {
                        key = key.derive(data.prev_outputs[i].branch);
                        if (data.prev_outputs[i].subaccount) {
                            key = key.derivePrivate(data.prev_outputs[i].subaccount);
                        }
                        key = key.derive(data.prev_outputs[i].pointer);
                    }
                    key = key.priv;
                }
                var script = new Bitcoin.Script(Bitcoin.convert.hexToBytes(data.prev_outputs[i].script));
                var SIGHASH_ALL = 1;
                var sign = key.sign(tx.hashTransactionForSignature(script, i, SIGHASH_ALL));
                sign.push(SIGHASH_ALL);
                signatures.push(Bitcoin.convert.bytesToHex(sign));
            }
            tx_sender.call("http://greenaddressit.com/vault/send_tx", signatures, twofactor||null).then(function(data) {
                d.resolve();
                if (!twofactor) {
                    tx_sender.call("http://greenaddressit.com/login/get_spending_limits").then(function(data) {
                        $scope.wallet.limits.total = data.total;
                    });
                }
                if (notify !== false) {
                    sound.play(BASE_URL + "/static/sound/coinsent.mp3", $scope);
                    notices.makeNotice('success', notify || gettext('Bitcoin transaction sent!'));
                }
            }, function(reason) {
                d.reject();
                notices.makeNotice('error', gettext('Transaction failed: ') + reason.desc);
                sound.play(BASE_URL + "/static/sound/wentwrong.mp3", $scope);
            });
            return d.promise;
        }
        walletsService.getTwoFacConfig = function($scope, force) {
            var d = $q.defer();
            if ($scope.wallet.twofac !== undefined && !force) {
                d.resolve($scope.wallet.twofac);
            } else {
                tx_sender.call('http://greenaddressit.com/twofactor/get_config').then(function(data) {
                    $scope.wallet.twofac = data;
                    d.resolve($scope.wallet.twofac);
                });
            }
            return d.promise;
        };
        walletsService.get_two_factor_code = function($scope, action, data) {
            var deferred = $q.defer();
            walletsService.getTwoFacConfig($scope).then(function(twofac_data) {
                if (twofac_data.any) {
                    $scope.twofactor_method_names = {
                        'gauth': 'Google Authenticator',
                        'email': 'Email',
                        'sms': 'SMS',
                        'phone': gettext('Phone')
                    };
                    $scope.twofactor_methods = [];
                    for (var key in $scope.twofactor_method_names) {
                        if (twofac_data[key] === true) {
                            $scope.twofactor_methods.push(key);
                        }
                    };
                    var order = ['gauth', 'email', 'sms', 'phone'];
                    $scope.twofactor_methods.sort(function(a,b) { return order.indexOf(a)-order.indexOf(b); })
                    $scope.twofac = {
                        twofactor_method: $scope.twofactor_methods[0],
                        codes_requested: {},
                        request_code: function() {
                            var that = this;
                            this.requesting_code = true;
                            return tx_sender.call('http://greenaddressit.com/twofactor/request_' + this.twofactor_method,
                                    action, data).then(function() {
                                    that.codes_requested[that.twofactor_method] = true;
                                    that.requesting_code = false;
                                }, function(err) {
                                    notices.makeNotice('error', err.desc);
                                    that.requesting_code = false;
                                });
                        }};
                    var show_modal = function() {
                        var modal = $modal.open({
                            templateUrl: BASE_URL+'/'+LANG+'/wallet/partials/wallet_modal_2fa.html',
                            scope: $scope,
                            windowClass: 'twofactor'
                        });
                        modal.opened.then(function() { focus("twoFactorModal"); });
                        deferred.resolve(modal.result);
                    };
                    if ($scope.twofactor_methods.length == 1) {
                        if ($scope.twofactor_methods[0] == 'gauth') {
                            // just gauth - no request required
                            $scope.twofac.gauth_only = true;  // don't display the radio buttons
                            // (not required in 'else' because codes_requested takes care of it)
                            show_modal();
                        } else {
                            // just sth else than gauth - request it because user can't choose anything else anyway
                            $scope.twofac.twofactor_method = $scope.twofactor_methods[0];
                            $scope.twofac.request_code().then(function() {
                                show_modal();
                            })
                        }
                    } else {
                        // more than one auth method available - allow the user to select
                        show_modal();
                    }
                } else {
                    return deferred.resolve(null);
                }
            });
            return deferred.promise;
        }
        walletsService.sign_and_send_tx = function($scope, data, priv_der, twofac_data, notify) {
            if ($scope && data.requires_2factor) {
                var d = $q.defer();
                walletsService.get_two_factor_code($scope, 'send_tx').then(function(twofac_data) {
                    d.resolve(_sign_and_send_tx($scope, data, priv_der, twofac_data, notify));
                }, function(err) { d.reject(err); });
                return d.promise;
            } else {
                return _sign_and_send_tx($scope, data, priv_der, twofac_data, notify);
            }
        }
        walletsService.addCurrencyConversion = function($scope, model_name) {
            var div = {'BTC': 1, 'mBTC': 1000, 'µBTC': 1000000}[$scope.wallet.unit];
            $scope.$watch(model_name+'.amount', function(newValue, oldValue) {
                if (newValue === oldValue) return;
                if ($scope[model_name].updated_by_conversion) {
                    $scope[model_name].updated_by_conversion = false;
                } else {
                    var oldFiat = $scope[model_name].amount_fiat;
                    if (!newValue) {
                        $scope[model_name].amount_fiat = '';
                    } else {
                        $scope[model_name].amount_fiat = newValue * $scope.wallet.fiat_rate / div;
                        $scope[model_name].amount_fiat = (Math.round($scope[model_name].amount_fiat * 100) / 100).toString();
                    }
                    if ($scope[model_name].amount_fiat !== oldFiat) {
                        $scope[model_name].updated_by_conversion = true;
                    }
                }
            });
            $scope.$watch(model_name+'.amount_fiat', function(newValue, oldValue) {
                if (newValue === oldValue) return;
                if ($scope[model_name].updated_by_conversion) {
                    $scope[model_name].updated_by_conversion = false;
                } else {
                    var oldBTC = $scope[model_name].amount;
                    if (!newValue) {
                        $scope[model_name].amount = '';
                    } else {
                        $scope[model_name].amount = (div * newValue / $scope.wallet.fiat_rate).toString();
                    }
                    if ($scope[model_name].amount !== oldBTC) {
                        $scope[model_name].updated_by_conversion = true;
                    }
                }
            });
        };
        walletsService.create_pin = function(pin, $scope) {
            var do_create = function() {
                var deferred = $q.defer();
                tx_sender.call('http://greenaddressit.com/pin/set_pin_login', pin, 'Primary').then(
                    function(data) {
                        if (data) {
                            var pin_ident = tx_sender.pin_ident = data;
                            storage.set('pin_ident', pin_ident);
                            tx_sender.call('http://greenaddressit.com/pin/get_password', pin, data).then(
                                function(password) {
                                    if (!$scope.wallet.hdwallet.seed_hex) {
                                        deferred.reject(gettext('Internal error')+': Missing seed');
                                        return;
                                    }
                                    if (password) {
                                        var data = JSON.stringify({'seed': $scope.wallet.hdwallet.seed_hex,
                                            'path_seed': $scope.wallet.gait_path_seed,
                                            'mnemonic': $scope.wallet.mnemonic});
                                        crypto.encrypt(data, password).then(function(encrypted) {
                                            storage.set('encrypted_seed', encrypted);
                                        });
                                        tx_sender.pin = pin;
                                        deferred.resolve(pin_ident);
                                    } else {
                                        deferred.reject(gettext('Failed retrieving password.'))
                                    }
                                }, function(err) {
                                    deferred.reject(err.desc);
                                });
                        } else {
                            deferred.reject();
                        }
                    }, function(err) {
                        deferred.reject(err.desc);
                    }
                );
                return deferred.promise;
            };
            if (!tx_sender.logged_in) {
                var hdwallet = Bitcoin.HDWallet.fromSeedHex($scope.wallet.hdwallet.seed_hex, cur_net);
                hdwallet.seed_hex = $scope.wallet.hdwallet.seed_hex;
                return walletsService.login($scope||{wallet:{}}, hdwallet,
                        $scope.wallet.mnemonic, false, false, $scope.wallet.gait_path_seed).then(function() {
                        return do_create();
                    });
            } else {  // already logged in
                return do_create();
            }
        };
        walletsService.askForLogout = function($scope, text) {
            $scope.ask_for_logout_text = text;
            return $modal.open({
                templateUrl: BASE_URL+'/'+LANG+'/wallet/partials/wallet_modal_logout.html',
                scope: $scope
            }).result;
        };
        return walletsService;
    }])
