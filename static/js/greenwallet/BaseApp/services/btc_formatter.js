/**
 * Created by PhpStorm.
 * User: Oleg Kudrenko <oleg.kudrenko@gmail.com>
 * Date: 7/28/14
 */
greenWalletBaseApp
    .factory('btc_formatter', function() {
        return function btc_formatter(satoshis, unit) {
            var mul = {'µBTC': '1000000', 'mBTC': '1000', 'BTC': '1'};
            satoshis = (new Bitcoin.BigInteger((satoshis || 0).toString())).multiply(new Bitcoin.BigInteger(mul[unit] || mul['µBTC']));
            if (satoshis.compareTo(new Bitcoin.BigInteger('0')) < 0) {
                return '-'+Bitcoin.Util.formatValue(satoshis.multiply(new Bitcoin.BigInteger('-1')));
            } else {
                return Bitcoin.Util.formatValue(satoshis);
            }
        };
    });