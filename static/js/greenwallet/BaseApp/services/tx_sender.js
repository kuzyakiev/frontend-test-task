greenWalletBaseApp.factory('tx_sender', ['$q', '$rootScope', 'cordovaReady', '$http', 'notices', 'gaEvent', '$location', 'autotimeout', 'device_id',
    function($q, $rootScope, cordovaReady, $http, notices, gaEvent, $location, autotimeout, device_id) {
        ab._Deferred = $q.defer;
        var txSenderService = {};
        if (window.Electrum) {
            if (window.cordova) {
                txSenderService.electrum = new Electrum($http, $q);
            } else {
                txSenderService.electrum = new Electrum();
                txSenderService.electrum.connectToServer();
            }
        }
        var session, calls = [];
        txSenderService.call = function() {
            var d = $q.defer();
            if (session) {
                session.call.apply(session, arguments).then(function(data) {
                    $rootScope.$apply(function() { d.resolve(data); })
                }, function(err) {
                    $rootScope.$apply(function() { d.reject(err); })
                });
            } else {
                if (disconnected) {
                    disconnected = false;
                    connect();
                }
                calls.push([arguments, d]);
            }
            return d.promise;
        };
        var isMobile = /Android|iPhone|iPad|iPod|Opera Mini/i.test(navigator.userAgent);
        if (window.cordova) {
            cordovaReady(function() {
                document.addEventListener("resume", function() {
                    if (!txSenderService.wallet) return;
                    session.close();  // reconnect on resume
                    session = null;
                    disconnected = true;
                    txSenderService.wallet.update_balance();
                    txSenderService.wallet.refresh_transactions();
                }, false);
            })();
        } else if (isMobile && typeof document.addEventListener !== undefined) {
            // reconnect on tab shown in mobile browsers
            document.addEventListener("visibilitychange", function() {
                if (!document.hidden && txSenderService.wallet) {
                    txSenderService.wallet.update_balance();
                    txSenderService.wallet.refresh_transactions();
                }
            }, false);
        }
        var onAuthed = function(s) {
            session = s;
            session.subscribe('http://greenaddressit.com/tx_notify', function(topic, event) {
                gaEvent('Wallet', 'TransactionNotification');
                $rootScope.$broadcast('transaction', event);        });
            session.subscribe('http://greenaddressit.com/block_count', function(topic, event) {
                $rootScope.$broadcast('block', event);
            });
            var d1, d2;
            if (txSenderService.hdwallet && txSenderService.logged_in) {
                txSenderService.logged_in = false;
                d1 = txSenderService.login();
            } else if (txSenderService.watch_only) {
                d1 = txSenderService.loginWatchOnly(txSenderService.watch_only[0], txSenderService.watch_only[1]);
            } else {
                d1 = $q.when(true);
            }
            d1.catch(function(err) {
                if (err.uri == 'http://greenaddressit.com/error#doublelogin') {
                    autotimeout.stop();
                    if (txSenderService.wallet) txSenderService.wallet.clear();
                    $location.path('/concurrent_login');
                } else {
                    notices.makeNotice('error', gettext('An error has occured which forced us to log you out.'))
                    if (txSenderService.wallet) txSenderService.wallet.clear();
                    $location.path('/');
                }
            });
            if (txSenderService.pin_ident) {
                // resend PIN to allow PIN changes in the event of reconnect
                d2 = session.call('http://greenaddressit.com/pin/get_password',
                    txSenderService.pin, txSenderService.pin_ident);
            } else {
                d2 = $q.when(true);
            }
            $q.all([d1, d2]).then(function() {
                // missed calls queue
                while (calls.length) {
                    var item = calls.shift();
                    item[1].resolve(txSenderService.call.apply(session, item[0]));
                }
            }, function(err) {
                // missed calls queue - reject them as well
                // safeApply because txSenderService.login might've called $apply already
                $rootScope.safeApply(function() {
                    while (calls.length) {
                        var item = calls.shift();
                        item[1].reject(err);
                    }
                });
            });
        };
        var retries = 60, everConnected = false, disconnected = false;
        var connect = function() {
            ab.connect(wss_url,
                function(s) {
                    everConnected = true;
                    $http.get((window.root_url||'')+'/token/').then(function(response) {
                        var token = response.data;
                        s.authreq(token).then(function(challenge) {
                            var signature = s.authsign(challenge, token);
                            s.auth(signature).then(function(permissions) {
                                onAuthed(s);
                            });
                        });
                    });
                },
                function(code, reason) {
                    if (retries && !everConnected) {  // autobahnjs doesn't reconnect automatically if it never managed to connect
                        retries -= 1;
                        setTimeout(connect, 5000);
                        return;
                    }
                    if (reason && reason.indexOf('WS-4000') != -1) {
                        $rootScope.$apply(function() {
                            autotimeout.stop();
                            txSenderService.logout();
                            $location.path('/concurrent_login');
                        });
                    }
                    if (reason && reason.indexOf('WS-4001') != -1) {  // concurrent login on the same device
                        $rootScope.$apply(function() {
                            autotimeout.stop();
                            txSenderService.logout();
                            $location.path('/');
                        });
                    }
                    session = null;
                },
                {maxRetries: 60}
            );
        };
        cordovaReady(connect)();
        txSenderService.logged_in = false;
        txSenderService.login = function(logout) {
            var d = $q.defer();
            if (txSenderService.logged_in) {
                d.resolve(txSenderService.logged_in);
            } else {
                var hdwallet = txSenderService.hdwallet;
                if (hdwallet) {
                    txSenderService.call('http://greenaddressit.com/login/get_challenge',
                            hdwallet.getAddress().toString()).then(function(challenge) {
                            var challenge_bytes = new Bitcoin.BigInteger(challenge).toByteArrayUnsigned();

                            // generate random path to derive key from - avoids signing using the same key twice
                            var max64int_hex = '';
                            while (max64int_hex.length < 16) max64int_hex += 'F';
                            var TWOPOWER64 = new Bitcoin.BigInteger(max64int_hex, 16).add(Bitcoin.BigInteger.ONE);
                            var random_path_hex = Bitcoin.ecdsa.getBigRandom(TWOPOWER64).toString(16);
                            while (random_path_hex.length < 16) random_path_hex = '0' + random_path_hex;
                            $q.when(hdwallet.subpath_for_login(random_path_hex)).then(function(subhd) {
                                $q.when(subhd.priv.sign(challenge_bytes)).then(function(signature) {
                                    signature = Bitcoin.ecdsa.parseSig(signature);
                                    d.resolve(device_id().then(function(devid) {
                                        return txSenderService.call('http://greenaddressit.com/login/authenticate',
                                                [signature.r.toString(), signature.s.toString()], logout||false,
                                                random_path_hex, devid).then(function(data) {
                                                txSenderService.logged_in = data;
                                                return data;
                                            });
                                    }));
                                });
                            });
                        });
                } else {  // trezor_dev
                    var trezor_dev = txSenderService.trezor_dev;
                    trezor_dev._typedCommonCall('GetAddress', 'Address', {
                        address_n: [],
                        coin_name: cur_net == 'testnet' ? 'Testnet' : 'Bitcoin'
                    }).then(function (addr) {
                        txSenderService.call('http://greenaddressit.com/login/get_trezor_challenge',
                                addr.message.address).then(function(challenge) {

                                msg = Bitcoin.CryptoJS.enc.Hex.stringify(Bitcoin.CryptoJS.enc.Utf8.parse('greenaddress.it      login ' + challenge))
                                // generate random path to derive key from - avoids signing using the same key twice
                                var max64int_hex = '';
                                while (max64int_hex.length < 16) max64int_hex += 'F';
                                var TWOPOWER64 = new Bitcoin.BigInteger(max64int_hex, 16).add(Bitcoin.BigInteger.ONE);
                                var random_path_hex = Bitcoin.ecdsa.getBigRandom(TWOPOWER64).toString(16);
                                while (random_path_hex.length < 16) random_path_hex = '0' + random_path_hex;
                                var path_bytes = Bitcoin.convert.hexToBytes(random_path_hex);
                                var path = [];
                                for (var i = 0; i < 4; i++) {
                                    path.push(+Bitcoin.BigInteger.fromByteArrayUnsigned(path_bytes.slice(0, 2)))
                                    path_bytes.shift(); path_bytes.shift();
                                }
                                trezor_dev.signing = true;
                                trezor_dev._typedCommonCall('SignMessage', 'MessageSignature',
                                    {'message': msg, address_n: path}).then(function(res) {
                                        signature = Bitcoin.ecdsa.parseSigCompact(Bitcoin.convert.hexToBytes(res.message.signature));
                                        trezor_dev.signing = false;
                                        d.resolve(device_id().then(function(devid) {
                                            return txSenderService.call('http://greenaddressit.com/login/authenticate',
                                                    [signature.r.toString(), signature.s.toString(), signature.i.toString()], logout||false,
                                                    random_path_hex, devid).then(function(data) {
                                                    txSenderService.logged_in = data;
                                                    return data;
                                                });
                                        }));
                                    }, function(err) {
                                        d.reject(err.message);
                                        trezor_dev.signing = false;
                                    });
                            });
                    });
                }
            }
            return d.promise;
        };
        txSenderService.logout = function() {
            if (session) {
                session.close();
            }
            disconnected = true;
            txSenderService.logged_in = false;
            txSenderService.hdwallet = undefined;
            txSenderService.trezor_dev = undefined;
            txSenderService.watch_only = undefined;
            if (txSenderService.wallet) txSenderService.wallet.clear();
        };
        txSenderService.loginWatchOnly = function(token_type, token, logout) {
            var d = $q.defer();
            txSenderService.call('http://greenaddressit.com/login/watch_only',
                    token_type, token, logout||false).then(function(data) {
                    txSenderService.watch_only = [token_type, token];
                    d.resolve(data);
                }, function(err) {
                    d.reject(err);
                });
            return d.promise;
        };
        txSenderService.change_pin = function(new_pin) {
            return txSenderService.call('http://greenaddressit.com/pin/change_pin_login',
                    new_pin, txSenderService.pin_ident).then(function() {
                    // keep new pin for reconnection handling
                    txSenderService.pin = new_pin;
                });
        };
        return txSenderService;
    }]);
