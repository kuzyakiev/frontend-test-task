/**
 * Created by PhpStorm.
 * User: Oleg Kudrenko <oleg.kudrenko@gmail.com>
 * Date: 7/28/14
 */
greenWalletBaseApp
    .factory('$exceptionHandler', ['$injector', '$log', function($injector, $log) {
        return function (exception, cause) {
            if (typeof exception == "string") {
                $injector.get('notices').makeNotice('error', exception);
            } else {
                $log.error.apply($log, arguments);
            }
        };
    }]);